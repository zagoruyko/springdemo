package com.example.demo.repository;

import com.example.demo.domain.ScienceDegree;
import org.springframework.data.repository.CrudRepository;

public interface ScienceDegreeRepo extends CrudRepository<ScienceDegree, Integer> {
}

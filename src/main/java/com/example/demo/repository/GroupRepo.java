package com.example.demo.repository;

import com.example.demo.domain.Group;
import org.springframework.data.repository.CrudRepository;

public interface GroupRepo extends CrudRepository<Group, Integer> {
}

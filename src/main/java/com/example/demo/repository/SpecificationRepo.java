package com.example.demo.repository;

import com.example.demo.domain.Specification;
import org.springframework.data.repository.CrudRepository;

public interface SpecificationRepo extends CrudRepository<Specification, Integer> {
}

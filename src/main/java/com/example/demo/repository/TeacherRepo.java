package com.example.demo.repository;

import com.example.demo.domain.Teacher;
import org.springframework.data.repository.CrudRepository;

public interface TeacherRepo extends CrudRepository<Teacher, Integer> {
}

package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import static com.example.demo.util.Constants.GROUP_ID;

@Data
public class StudentCreationDTO {

    private String name;
    private Integer course;
    @JsonProperty(GROUP_ID)
    private Integer groupId;
}

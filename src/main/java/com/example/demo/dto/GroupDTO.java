package com.example.demo.dto;

import com.example.demo.domain.Specification;
import com.example.demo.domain.Student;
import lombok.Data;

import java.util.List;

@Data
public class GroupDTO {

    private Integer id;
    private String label;
    private Specification specification;
    private List<Student> students;
}

package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import static com.example.demo.util.Constants.SCIENCE_DEGREE_ID;

@Data
public class TeacherCreationDTO {

    private String name;
    private Integer age;
    @JsonProperty(SCIENCE_DEGREE_ID)
    private Integer scienceDegreeId;
}

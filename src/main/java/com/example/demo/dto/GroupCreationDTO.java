package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import static com.example.demo.util.Constants.SPECIFICATION_ID;

@Data
public class GroupCreationDTO {

    private String label;
    @JsonProperty(SPECIFICATION_ID)
    private Integer specificationId;
}

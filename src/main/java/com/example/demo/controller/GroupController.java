package com.example.demo.controller;

import com.example.demo.dto.GroupCreationDTO;
import com.example.demo.dto.GroupDTO;
import com.example.demo.service.GroupService;
import com.example.demo.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(path = "/university/groups/",
        produces = "application/json",
        consumes = "application/json")
public class GroupController {

    private final GroupService groupService;

    @Autowired
    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping
    public ResponseEntity<Iterable<GroupDTO>> groups() {
        return ResponseUtil.getValidIterableResponse(groupService.getGroups());
    }

    @GetMapping("/{id}")
    public ResponseEntity<GroupDTO> groupById(@PathVariable("id") Integer id) {
        Optional<GroupDTO> optGroupDTO = groupService.getGroupDTOById(id);
        return optGroupDTO.map(groupDTO -> new ResponseEntity<>(groupDTO, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(null, HttpStatus.NOT_FOUND));
    }

    @PostMapping(value = "add/")
    @ResponseStatus(HttpStatus.CREATED)
    public GroupDTO addGroup(@RequestBody GroupCreationDTO groupCreationDTO) {
        return groupService.addGroup(groupCreationDTO);
    }
}

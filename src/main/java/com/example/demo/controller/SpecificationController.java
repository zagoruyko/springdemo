package com.example.demo.controller;

import com.example.demo.domain.Specification;
import com.example.demo.service.SpecificationService;
import com.example.demo.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/university/specifications/",
        produces = "application/json",
        consumes = "application/json")
public class SpecificationController {

    private final SpecificationService specifService;

    @Autowired
    public SpecificationController(SpecificationService specifService) {
        this.specifService = specifService;
    }

    @GetMapping
    public ResponseEntity<Iterable<Specification>> specifications() {
        return ResponseUtil.getValidIterableResponse(specifService.getAll());
    }

    @PostMapping(value = "add/")
    @ResponseStatus(HttpStatus.CREATED)
    public Specification addSpecification(@RequestBody Specification specification) {
        return specifService.save(specification);
    }
}

package com.example.demo.controller;

import com.example.demo.domain.Teacher;
import com.example.demo.dto.TeacherCreationDTO;
import com.example.demo.service.TeacherService;
import com.example.demo.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/university/teachers/",
        produces = "application/json",
        consumes = "application/json")
public class TeacherController {

    private final TeacherService teacherService;

    @Autowired
    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping
    public ResponseEntity<Iterable<Teacher>> groups() {
        return ResponseUtil.getValidIterableResponse(teacherService.getTeachers());
    }

    @PostMapping(value = "add/")
    @ResponseStatus(HttpStatus.CREATED)
    public Teacher addTeacher(@RequestBody TeacherCreationDTO creationDTO) {
        return teacherService.addTeacher(creationDTO);
    }
}

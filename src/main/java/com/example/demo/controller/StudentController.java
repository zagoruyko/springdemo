package com.example.demo.controller;

import com.example.demo.domain.Student;
import com.example.demo.dto.StudentCreationDTO;
import com.example.demo.service.StudentService;
import com.example.demo.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/university/students/",
        produces = "application/json",
        consumes = "application/json")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public ResponseEntity<Iterable<Student>> students() {
        return ResponseUtil.getValidIterableResponse(studentService.getStudents());
    }

    @PostMapping(value = "add/")
    @ResponseStatus(HttpStatus.CREATED)
    public Student addStudent(@RequestBody StudentCreationDTO creationDTO) {
        return studentService.addStudent(creationDTO);
    }
}

package com.example.demo.controller;

import com.example.demo.domain.ScienceDegree;
import com.example.demo.dto.ScienceDegreeCreationDTO;
import com.example.demo.service.ScienceDegreeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/university/science-degrees/",
        produces = "application/json",
        consumes = "application/json")
public class ScienceDegreeController {

    private ScienceDegreeService scienceDegreeService;

    @Autowired
    public ScienceDegreeController(ScienceDegreeService scienceDegreeService) {
        this.scienceDegreeService = scienceDegreeService;
    }

    @GetMapping()
    public ResponseEntity<Iterable<ScienceDegree>> scienceDegrees() {
        return new ResponseEntity<>(scienceDegreeService.getScienceDegrees(),
                HttpStatus.OK);
    }

    @PostMapping(value = "add/")
    @ResponseStatus(HttpStatus.CREATED)
    public ScienceDegree addScienceDegree(@RequestBody ScienceDegreeCreationDTO creationDTO) {
        return scienceDegreeService.addScienceDegree(creationDTO);
    }
}

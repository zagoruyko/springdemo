package com.example.demo.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.collections4.IterableUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ResponseUtil {

    public static <V> ResponseEntity<Iterable<V>> getValidIterableResponse(Iterable<V> iterable) {
        if (IterableUtils.size(iterable) == 0) {
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(iterable, HttpStatus.OK);
    }
}

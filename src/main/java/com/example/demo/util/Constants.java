package com.example.demo.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Constants {

    public static final String SPECIFICATION_ID = "specification_id";
    public static final String GROUP_ID = "group_id";
    public static final String SCIENCE_DEGREE_ID = "science_degree_id";
}

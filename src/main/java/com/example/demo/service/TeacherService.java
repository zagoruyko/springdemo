package com.example.demo.service;

import com.example.demo.domain.ScienceDegree;
import com.example.demo.domain.Teacher;
import com.example.demo.dto.TeacherCreationDTO;
import com.example.demo.repository.TeacherRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeacherService {

    private final TeacherRepo teacherRepo;
    private final ModelMapper modelMapper;

    private ScienceDegreeService scienceDegreeService;

    @Autowired
    public TeacherService(TeacherRepo teacherRepo,
                          ModelMapper modelMapper) {
        this.teacherRepo = teacherRepo;
        this.modelMapper = modelMapper;
    }

    @Autowired
    public void setScienceDegreeService(ScienceDegreeService scienceDegreeService) {
        this.scienceDegreeService = scienceDegreeService;
    }

    public Iterable<Teacher> getTeachers() {
        return teacherRepo.findAll();
    }

    public Teacher addTeacher(TeacherCreationDTO creationDTO) {
        Teacher teacher = convertToEntity(creationDTO);
        return teacherRepo.save(teacher);
    }

    private ScienceDegree getScienceDegreeById(Integer id) {
        return scienceDegreeService.getScienceDegreeById(id);
    }

    private Teacher convertToEntity(TeacherCreationDTO creationDTO) {
        Teacher teacher = modelMapper.map(creationDTO, Teacher.class);
        teacher.setScienceDegree(getScienceDegreeById(creationDTO.getScienceDegreeId()));
        return teacher;
    }
}

package com.example.demo.service;

import com.example.demo.domain.Student;
import com.example.demo.dto.StudentCreationDTO;
import com.example.demo.repository.StudentRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService {

    private final StudentRepo studentRepo;
    private final ModelMapper modelMapper;

    @Autowired
    public StudentService(StudentRepo studentRepo,
                          ModelMapper modelMapper) {
        this.studentRepo = studentRepo;
        this.modelMapper = modelMapper;
    }

    public Iterable<Student> getStudents() {
        return studentRepo.findAll();
    }

    public Student addStudent(StudentCreationDTO creationDTO) {
        Student student = convertToEntity(creationDTO);
        return studentRepo.save(student);
    }

    private Student convertToEntity(StudentCreationDTO creationDTO) {
        return modelMapper.map(creationDTO, Student.class);
    }
}

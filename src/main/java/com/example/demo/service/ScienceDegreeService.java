package com.example.demo.service;

import com.example.demo.domain.ScienceDegree;
import com.example.demo.dto.ScienceDegreeCreationDTO;
import com.example.demo.repository.ScienceDegreeRepo;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScienceDegreeService {

    private final ScienceDegreeRepo scienceDegreeRepo;
    private final ModelMapper modelMapper;

    @Autowired
    public ScienceDegreeService(ScienceDegreeRepo scienceDegreeRepo, ModelMapper modelMapper) {
        this.scienceDegreeRepo = scienceDegreeRepo;
        this.modelMapper = modelMapper;
    }

    public Iterable<ScienceDegree> getScienceDegrees() {
        return scienceDegreeRepo.findAll();
    }

    public ScienceDegree addScienceDegree(ScienceDegreeCreationDTO creationDTO) {
        ScienceDegree scienceDegree = convertToEntity(creationDTO);
        return scienceDegreeRepo.save(scienceDegree);
    }

    public ScienceDegree getScienceDegreeById(Integer id) {
        return scienceDegreeRepo.findById(id).orElse(null);
    }

    private ScienceDegree convertToEntity(ScienceDegreeCreationDTO creationDTO) {
        return modelMapper.map(creationDTO, ScienceDegree.class);
    }
}

package com.example.demo.service;

import com.example.demo.domain.Group;
import com.example.demo.domain.Specification;
import com.example.demo.dto.GroupCreationDTO;
import com.example.demo.dto.GroupDTO;
import com.example.demo.repository.GroupRepo;
import org.apache.commons.collections4.IterableUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class GroupService {

    private final GroupRepo groupRepo;
    private final ModelMapper modelMapper;

    private SpecificationService specifService;
    private StudentService studentService;

    @Autowired
    public GroupService(GroupRepo groupRepo,
                        ModelMapper modelMapper) {
        this.groupRepo = groupRepo;
        this.modelMapper = modelMapper;
    }

    @Autowired
    public void setSpecifService(SpecificationService specifService) {
        this.specifService = specifService;
    }

    @Autowired
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    public Iterable<GroupDTO> getGroups() {
        return IterableUtils.toList(groupRepo.findAll())
                .stream()
                .map(this::convertToDTO)
                .collect(Collectors.toList());
    }

    public Optional<GroupDTO> getGroupDTOById(Integer id) {
        return Optional.of(convertToDTO(groupRepo.findById(id).orElse(null)));
    }

    public GroupDTO addGroup(GroupCreationDTO creationDTO) {
        Group group = convertToEntity(creationDTO);
        group.setSpecification(getSpecif(creationDTO.getSpecificationId()));
        return convertToDTO(groupRepo.save(group));
    }

    private Specification getSpecif(Integer id) {
        return specifService.getSpecifById(id).orElse(null);
    }

    private Group convertToEntity(GroupCreationDTO creationDTO) {
        return modelMapper.map(creationDTO, Group.class);
    }

    private GroupDTO convertToDTO(Group group) {
        GroupDTO groupDTO = modelMapper.map(group, GroupDTO.class);
        groupDTO.setStudents(IterableUtils.toList(
                studentService.getStudents()).stream()
                .filter(student ->
                        student.getGroupId().equals(group.getId()))
                .collect(Collectors.toList()));
        return groupDTO;
    }
}

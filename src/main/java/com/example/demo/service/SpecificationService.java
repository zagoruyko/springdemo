package com.example.demo.service;

import com.example.demo.domain.Specification;
import com.example.demo.repository.SpecificationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class SpecificationService {

    private final SpecificationRepo specifRepo;

    @Autowired
    public SpecificationService(SpecificationRepo specifRepo) {
        this.specifRepo = specifRepo;
    }

    public Iterable<Specification> getAll() {
        return specifRepo.findAll();
    }

    public Optional<Specification> getSpecifById(Integer id) {
        return specifRepo.findById(id);
    }

    public Specification save(Specification specif) {
        return specifRepo.save(specif);
    }
}
